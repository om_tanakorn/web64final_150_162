import React from 'react'

import { Box } from '@mui/material'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import { makeStyles } from '@mui/styles'

import { brown } from '@mui/material/colors'
import { styled } from '@mui/material/styles'
import Button from '@mui/material/Button'

import Post from './Post/PostTheTernCafe'

const useStyles = makeStyles((theme) => ({
    titles: {
        fontFamily: 'Nunito',
        fontWeight: 'bold',
        fontSize: '2rem',
        color: '#fff'
    },
    desc: {
        fontFamily: 'Nunito',
        fontSize: '1.1rem',
        color: '#ddd'
    },
    textarea: {
        fontFamily: 'Nunito',
    }
}))

const ColorButton = styled(Button)(({ theme }) => ({
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    fontSize: '1.1rem',
    color: theme.palette.getContrastText(brown[800]),
    backgroundColor: brown[800],
    '&:hover': {
      backgroundColor: brown[700],
    },
}))

function TheTernCafe() {
    const classes = useStyles()
    return (
        <Box 
         sx={{
            minHeight: '100vh',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
         }}
        >
            <Card sx={{ 
                maxWidth: 1000,
                background: 'rgba(0, 0, 0, 0.5)',
                margin: '10px'  
                }}>
                <CardMedia
                sx={{ height: 440 }}
                image={process.env.PUBLIC_URL + '/assets/TheTernCafe.jpg'}
                title="Contemplative Reptile"
                />
                <CardContent>
                <h1 className={classes.titles}>
                    The Tern Cafe
                </h1>
                <h6 className={classes.desc}>
                    3.6 ★ / 21 reviews
                </h6>
                <h6 className={classes.desc}>
                    opening time
                </h6>
                <h6 className={classes.desc}>
                    Everydays	11:00 - 22:00
                </h6>
                <h6 className={classes.desc}>
                    Hat Yai Cafe is newly opened in Soi Rat Yindee 7/1. The big shop is a 2-storey glass building. There is a parking lot in front of the shop. The shop is airy, not uncomfortable. Sit comfortably, chill, have a beautiful photo corner. The food has both savory dishes. focus on one dish And there are snacks such as french fries, various salads. Drinks include tea, coffee, and fruit smoothies. We ordered “Bacon Dried Chili Spaghetti” and the fried noodles were just right. has the spiciness of dried chili And the crispy, salty of bacon. Service was good. Food came out quickly. The wait didn't take long.
                </h6>
                <h1 className={classes.titles}>
                    Comment
                </h1>
                <Post />
                </CardContent>
            </Card>
        </Box>
    )
}

export default TheTernCafe