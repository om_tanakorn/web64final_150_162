const cafe =[
    {
        title: 'Foresto',
        score:
            `4.2 ★ / 91 reviews`,
        description:
            `Another coffee shop in Hat Yai that you should try. Raw style, cool, close to nature. The shop is bare cement, prominently surrounded by lush green plants, blending in perfectly. with drinks at friendship prices The drink that we ordered today is "Hojicha Latte" Hojicha or roasted green tea, fragrant, sweet, just right, without having to ask for a discount, ask for anything to add to the chaos, service, lovely staff, good conversation, clean shop, beautiful photos, come on, you won't be disappointed.`,
        imageUrl: process.env.PUBLIC_URL + '/assets/Foresto.jpg',
        time: 1500,
    },
    {
        title: 'The Tern Cafe',
        score:
            `3.6 ★ / 21 reviews`,
        description:
            `Hat Yai Cafe is newly opened in Soi Rat Yindee 7/1. The big shop is a 2-storey glass building. There is a parking lot in front of the shop. The shop is airy, not uncomfortable. Sit comfortably, chill, have a beautiful photo corner. The food has both savory dishes. focus on one dish And there are snacks such as french fries, various salads. Drinks include tea, coffee, and fruit smoothies. We ordered “Bacon Dried Chili Spaghetti” and the fried noodles were just right. has the spiciness of dried chili And the crispy, salty of bacon. Service was good. Food came out quickly. The wait didn't take long.`,
        imageUrl: process.env.PUBLIC_URL + '/assets/TheTernCafe.jpg',
        time: 1500
    }
]

export default cafe