import React from 'react'
import { makeStyles } from '@mui/styles'
import { List, ListItem, ListItemText, ListItemAvatar, Avatar, Typography } from '@mui/material'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: 'white'
  },
  inline: {
    display: 'inline',
  },
}));

function CommentForesto() {
    const classes = useStyles();
    return (
        <List className={classes.root}>
            <ListItem alignItems="flex-start">
                <ListItemAvatar>
                    <Avatar alt="Remy Sharp" src="https://images.unsplash.com/photo-1609220361638-14ceb45e5e1e?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max" />
                </ListItemAvatar>
                <ListItemText
                 primary="username"
                 secondary={
                    <React.Fragment>
                    <Typography
                        component="span"
                        variant="body2"
                        className={classes.inline}
                        color="textPrimary"
                    >
                        White Choc Green Tea Served with hot red bean paste Cut with iced green tea ice cream. There is a fragrant whipped cream as topping on the side. Eat with hot chocolate. Get a good book. Sit back and enjoy. It helps our afternoons to be fresh. and relax a lot
                    </Typography>
                    </React.Fragment>
                 }
                />
            </ListItem>
        </List>
    )
}

export default CommentForesto