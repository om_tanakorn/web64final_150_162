-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 04, 2022 at 04:20 AM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CoffeeLove`
--

-- --------------------------------------------------------

--
-- Table structure for table `Post`
--

CREATE TABLE `Post` (
  `ShopID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Post` varchar(5000) NOT NULL,
  `PostTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='3';

--
-- Dumping data for table `Post`
--

INSERT INTO `Post` (`ShopID`, `UserID`, `Post`, `PostTime`) VALUES
(15, 5, 'ร้านน่านั่ง สาวเยอะ หนุ่มใจเกเรก็เยอะ แต่แพง อร่อย อเมริกาโน่ไม่ใส่น้ำตาลถูกต้องตามกฏหมาย', '2022-04-03 10:26:00'),
(16, 5, 'อร่อยดี บรรยากาศดี ราคาดี', '2022-04-03 16:44:07'),
(16, 7, 'อร่อยดี บรรยากาศดี ราคาดี', '2022-04-03 16:44:25');

-- --------------------------------------------------------

--
-- Table structure for table `Shop`
--

CREATE TABLE `Shop` (
  `ShopID` int(11) NOT NULL,
  `ShopName` varchar(200) NOT NULL,
  `Description` varchar(5000) NOT NULL,
  `ShopLocation` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Shop`
--

INSERT INTO `Shop` (`ShopID`, `ShopName`, `Description`, `ShopLocation`) VALUES
(15, 'Gray18 cafe`', 'เราใส่ใจคุณ', 'ติดโรงเรียนสภาราชะนี ถนน.วิเศษเกินไป จังหวัด.อะไรไม่รู้'),
(16, 'THE TERN CAFE', 'The Tern cafe คาเฟเอาใจสายกาแฟ เราคัดสันเมล็กกาแฟอาราบิก้า ผ่านกรมวิธีการคั่วด้วยเตาถ่าน และบดด้วย 2 มือร้านเปิดทำการเวลา 8.30-18.00 น.', 'หาดใหญ่ใกล้ มอ.( สมมุดติว่าเป็นที่อยู่) ตำบนคอหง'),
(17, 'FORESTO', 'ร้านกาแฟตกแต่งดี สไตลยาจก สั่ังกาแฟมากจากเอธิโอเปีย คุณภาพคับแก้วพร้อมเสิฟถึงมือคุณ เปิดทำการเวลา 9.00-20.00 น.', 'บิก C หาดใหญ่ ใกล้มอ. เลี้ยวขวาถึงเลย');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `UserID` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `Username` varchar(200) NOT NULL,
  `Password` varchar(500) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`UserID`, `Name`, `Username`, `Password`, `isAdmin`) VALUES
(4, 'test_admin', 'test0', '$2b$10$/0is1oE/GMS7AJHpVJDEu.QQ6Oyn2UhlxXSkbTrwELhfN3bRudGHW', 1),
(5, 'test_noadmin', 'test1', '$2b$10$O3LXibkAmw0P8K/aKidBrelvEvoOwR3tMh.ZJOcNOIyFnUOQxKgcq', 0),
(6, 'OM', 'test2', '$2b$10$VPY88ht6wzkoCGm3grD2YOSRBSnJ97mGWuElQHwCjWrvJdCsU1Sq6', 0),
(7, 'DOG', 'test3', '$2b$10$U3eoM.aYjzUWrs8Hc8OOue0iBGT5n1j2YU48rKQ2V/iVXR3jdb466', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Post`
--
ALTER TABLE `Post`
  ADD KEY `ShopID` (`ShopID`),
  ADD KEY `UserID` (`UserID`);

--
-- Indexes for table `Shop`
--
ALTER TABLE `Shop`
  ADD PRIMARY KEY (`ShopID`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`UserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Shop`
--
ALTER TABLE `Shop`
  MODIFY `ShopID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Post`
--
ALTER TABLE `Post`
  ADD CONSTRAINT `ShopID` FOREIGN KEY (`ShopID`) REFERENCES `Shop` (`ShopID`),
  ADD CONSTRAINT `UserID` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
