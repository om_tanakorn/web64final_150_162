import React from 'react'
import { Grid, Paper, Avatar, TextField, Button, Typography } from '@mui/material'
import { Link as FakeLink } from '@mui/material'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import { Link } from 'react-router-dom'

function Login() {
    const paperStyle={ padding : 50, height: '60vh', maxWidth: 645, margin: 'auto' }
    const avatarStyle={ backgroundColor:'#1bbd7e' }
    const btnstyle={ margin:'8px 0' }
    const marginTop = { marginTop: 10 }
    return (
        <Grid>
            <Paper elevation={10} style={paperStyle}>
                <Grid align='center'>
                    <Avatar style={avatarStyle}><LockOutlinedIcon/></Avatar>
                    <h2>Sign In</h2>
                </Grid>
                <TextField label='Username' placeholder='Enter username' fullWidth required/>
                <TextField style={marginTop} label='Password' placeholder='Enter password' type='password' fullWidth required/>
                <FormControlLabel
                    control={
                    <Checkbox
                        name="checkedB"
                        color="primary"
                    />
                    }
                    label="Remember me"
                />
                <Button 
                 type='submit' 
                 color='primary' 
                 variant="contained" 
                 style={btnstyle} 
                 fullWidth
                 component={ Link } to='PlaceCafe'
                >
                    Sign in
                </Button>
                <Typography >
                    <FakeLink herf='#'>
                        Forgot password ?
                    </FakeLink>
                </Typography>
                <Typography > Do you have an account ?
                    <Link to='Register'>
                        Sign Up 
                    </Link>
                </Typography>
            </Paper>
        </Grid>
    )
}

export default Login