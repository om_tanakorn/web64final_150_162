/* ===================== Token ====================== */
/* -------------------------------------------------- */
/*  ID test0 , 
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QwIiwidXNlcl9pZCI6NCwiSXNBZG1pbiI6MSwiaWF0IjoxNjQ4OTc3NzYxLCJleHAiOjE2NDkwNjQxNjF9.vDSlDr59Jrb0KoYT_CRzaLG7qz5_Z3MYlrEbfYz9vvs
    ID test1 ,
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QxIiwidXNlcl9pZCI6NSwiSXNBZG1pbiI6MCwiaWF0IjoxNjQ4OTc3NzQ3LCJleHAiOjE2NDkwNjQxNDd9.TNETO0uQs6wvX18HZtZGO12tq8Rlib-8PDcY-xTgTO4

/* ================================================== */
const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'coffee',
    password : 'lovecoffee',
    database : 'CoffeeLove'
})

connection.connect();
const express =require('express')
const jsonwebtoken = require('jsonwebtoken')
const app = express()
const port = 4000

/* ===================== Middlewar ================= */

function autheticateToken(req , res , next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null ) return res.sendStatus(401)
    jwt.verify(token,TOKEN_SECRET, (err,user) => {
        if(err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}
/* ================================================== 
                LOGIN SYSTEM & REGISTER
   ================================================== */

/* === LOGIN === */
app.get("/login" , (req,res) => {
    let username = req.query.username
    let password = req.query.password

    let query = `SELECT * FROM User WHERE Username = '${username}'`

    connection.query(query ,(err,row) => {
        if(err){
            console.log(err)
            res.json({
                "STATUS" : "400" ,
                "MESSAGE" : "Error noting in Databesssssssss "
            })
        }else{
            let db_password = row[0].Password
            bcrypt.compare(password, db_password , (err,result) => {
                if(result){
                    let payload = {
                        "username" : row[0].Username,
                        "user_id" : row[0].UserID,
                        "IsAdmin" : row[0].isAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload , TOKEN_SECRET , {expiresIn : '1d'})
                    res.send(token)
                }else {res.send("Inalid usernaem / password")}
            })
        }
    })
})

/* === Register === */
app.get("/register" , (req,res) => {
    let username = req.query.username
    let password = req.query.password
    let name = req.query.name

    /* = bcrypt = */
    bcrypt.hash(password,SALT_ROUNDS,(err,hash) => {
        let query = `INSERT INTO User
                    (Username,Password,Name,isAdmin)
                    VALUES ('${username}','${hash}','${name}',false)`

        console.log(query)
        connection.query(query,(err,rows) => {
            console.log(err)
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from coffee db"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "Add new user succesful"
            })
        }
        })
    })
})

/**************************************************** */
/* ================================================== 
                POST AND REVIWVE SHOP
   ================================================== */

/* === POST REVIVEW === */
app.get("/post_shop",autheticateToken,(req,res) => {
    let user_profile = req.user
    console.log(user_profile)

    let user_id = req.user.user_id
    let shop_id = req.query.shop_id
    let post = req.query.post

    let query = `INSERT INTO Post
                (ShopID,UserID,Post,PostTime)
                VALUES (${shop_id},
                        ${user_id},
                        '${post}',
                        NOW() )`

    console.log(query)
    connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from coffee db"
                })
        }else{
            res.json({
                    "status" : "200",
                    "message" : "succesful"
            });
        }
    })
})

/* === SELECT ONLY SHOP === */
/*SELECT 
    User.UserID,User.Name,Shop.ShopID,Shop.ShopName,
    Shop.ShopLocation,Post.Post,Post.PostTime
FROM 
    Post,Shop,User 
WHERE
    (Post.UserID = User.UserID)AND
    (Post.ShopID = Shop.ShopID)AND
    (Post.ShopID = 15);*/

app.get("/list_postbyshop",(req,res) => {
    let shop_id = req.query.shop_id

    let query = `
    SELECT 
        User.UserID,User.Name,Shop.ShopID,Shop.ShopName,
        Shop.ShopLocation,Post.Post,Post.PostTime
    FROM 
        Post,Shop,User
    WHERE
        (Post.UserID = User.UserID)AND
        (Post.ShopID = Shop.ShopID)AND
        (Post.ShopID = ${shop_id});
    `

    connection.query(query,(err,rows) => {
        if (err){
            res.json({
                    "status" : "400",
                    "message" : "Error querying from coffee db"
                })
        }else{
            res.json(rows)
        }
    })
})


/* ================================================== 
                    SHOP MANAGER
   ================================================== */

/* === LIST COFFEE === */
app.get("/list_shop",(req,res) => {
    let query = `SELECT * from Shop `
    connection.query(query,(err,row) => {
        if(err){
            res.json({
                "STATUS" : "400",
                "MESSAGE" : "Error querying from coffee db"
            })
        }else{
            res.json(row)
        }
    })
})

/* === ADD SHOP === */
app.post("/add_shop", autheticateToken , (req,res) => {
    let shop_name = req.query.shop_name
    let description = req.query.description
    let shop_location = req.query.shop_location

    if(!req.user.IsAdmin) {res.send("You not Admin")}
    else{
        let query = `INSERT INTO 
                    Shop (ShopName,Description,ShopLocation)
                    VALUES ('${shop_name}','${description}','${shop_location}' )`

    connection.query(query,(err,rows) =>{
        if(err){
            console.log(err)
            res.json({
                "STATUS" : "400",
                "MASSGE" : "Error can't add your coffee"
            })
        }else{
            res.json({
                "STATUS" : "200",
                "MASSGE" : `ADD succesful`
            })
            }
        })
    }
})

/* === UPDATE SHOP === */
app.post("/update_shop",autheticateToken , (req,res) => {
    let shop_id = req.query.shop_id
    let shop_name = req.query.shop_name
    let shop_location = req.query.shop_location
    let description = req.query.description

    if(!req.user.IsAdmin) {res.send("You not Admin")}
    else{
        let query = `UPDATE Shop SET
                        ShopName = '${shop_name}',
                        ShopLocation = '${shop_location}',
                        Description = '${description}'
                        WHERE ShopID = ${shop_id}`
        console.log(query)

        connection.query(query,(err,rows) =>{
            console.log(err)
            if (err){
                res.json({
                    "STATUS" : "400",
                    "MESSAGE" : "ERROR can't update coffee"
                })
            }else{
                res.json({
                    "STATUS" : "200",
                    "MESSAGE" : `Updating ${shop_id} succesful`
                })
            }
        })
    }
})
/* === DELETE SHOP === */
app.post("/delete_shop" , autheticateToken ,(req,res) => {

    let shop_id = req.query.shop_id

    if(!req.user.IsAdmin) {res.send("You not Admin")}
    else{
        let query = `DELETE FROM Shop WHERE ShopID=${shop_id}`

        connection.query(query,(err,row) => {
            if(err){
                res.json({
                    "status" : "400",
                    "message" : "Error queryign form shop db"
                })
            }else{
                res.json({
                    "STATUS" : "200",
                    "MESSAGE" : `Delete ${shop_id} succesful`
                })
            }
        })
    }
})

app.listen(port,() =>{
    console.log(`Now starting CoffeeLover System Backend ${port} `)
})