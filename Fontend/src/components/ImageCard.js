import React from 'react'
import { Box } from '@mui/material'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles((theme) => ({
    titles: {
        fontFamily: 'Nunito',
        fontWeight: 'bold',
        fontSize: '2rem',
        color: '#fff'
    },
    desc: {
        fontFamily: 'Nunito',
        fontSize: '1.1rem',
        color: '#ddd'
    },
}))

function ImageCard({ cafe }) {
    const classes = useStyles()
  return (
    <Box alignItems='center'>
      <Card sx={{ 
          maxWidth: 650,
          background: 'rgba(0, 0, 0, 0.5)',
          margin: '10px'  
          }}>
        <CardMedia
          sx={{ height: 440 }}
          image={cafe.imageUrl}
          title="Contemplative Reptile"
        />
        <CardContent>
          <h1 className={classes.titles}>
              {cafe.title}
          </h1>
          <h6 className={classes.desc}>
              {cafe.score}
          </h6>
          <h6 className={classes.desc}>
              {cafe.description}
          </h6>
        </CardContent>
      </Card>
    </Box>
  )
}

export default ImageCard