import { Routes, Route} from 'react-router-dom'
import { Box } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { CssBaseline } from '@mui/material'

import Header from './components/Header'
import Login from './components/Login'
import Register from './components/Register'
import PlaceCafe from './components/PlaceCafe'
import Foresto from './components/Foresto'
import TheTernCafe from './components/TheTernCafe'

const useStyles = makeStyles((theme) => ({
    root: {
      minHeight: '100vh',
      backgroundImage: `url(${process.env.PUBLIC_URL + '/assets/CoffeeBG.jpg'})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
    }
}))

function App() {
  const classes = useStyles()
  return (
    <Box className={classes.root}>
      <CssBaseline/>
      <Header />
      <Routes>
        <Route path='/' element={ <Login /> } exact={true} />
        <Route path='Register' element={ <Register /> } />
        <Route path='PlaceCafe/*' element={ <PlaceCafe /> } />
        <Route path='PlaceCafe/Foresto' element={ <Foresto /> } />
        <Route path='PlaceCafe/TheTernCafe' element={ <TheTernCafe /> } />
      </Routes>
    </Box>
  );
}

export default App