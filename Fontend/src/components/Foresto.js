import React from 'react'

import { Box } from '@mui/material'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import { makeStyles } from '@mui/styles'

import { brown } from '@mui/material/colors'
import { styled } from '@mui/material/styles'
import Button from '@mui/material/Button'

import PostTheTernCafe from '../components/Post/PostTheTernCafe'

const useStyles = makeStyles((theme) => ({
    titles: {
        fontFamily: 'Nunito',
        fontWeight: 'bold',
        fontSize: '2rem',
        color: '#fff'
    },
    desc: {
        fontFamily: 'Nunito',
        fontSize: '1.1rem',
        color: '#ddd'
    },
    textarea: {
        fontFamily: 'Nunito',
    }
}))

const ColorButton = styled(Button)(({ theme }) => ({
    fontFamily: 'Nunito',
    fontWeight: 'bold',
    fontSize: '1.1rem',
    color: theme.palette.getContrastText(brown[800]),
    backgroundColor: brown[800],
    '&:hover': {
      backgroundColor: brown[700],
    },
}))

function Foresto() {
    const classes = useStyles()
    return (
        <Box 
         sx={{
            minHeight: '100vh',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
         }}
        >
            <Card sx={{ 
                maxWidth: 1000,
                background: 'rgba(0, 0, 0, 0.5)',
                margin: '10px'  
                }}>
                <CardMedia
                sx={{ height: 440 }}
                image={process.env.PUBLIC_URL + '/assets/Foresto.jpg'}
                title="Contemplative Reptile"
                />
                <CardContent>
                <h1 className={classes.titles}>
                    Foresto
                </h1>
                <h6 className={classes.desc}>
                    4.2 ★ / 91 reviews
                </h6>
                <h6 className={classes.desc}>
                    opening time
                </h6>
                <h6 className={classes.desc}>
                    Everydays	07:00 - 20:00
                </h6>
                <h6 className={classes.desc}>
                    Another coffee shop in Hat Yai that you should try. Raw style, cool, close to nature. The shop is bare cement, prominently surrounded by lush green plants, blending in perfectly. with drinks at friendship prices The drink that we ordered today is "Hojicha Latte" Hojicha or roasted green tea, fragrant, sweet, just right, without having to ask for a discount, ask for anything to add to the chaos, service, lovely staff, good conversation, clean shop, beautiful photos, come on, you won't be disappointed.
                </h6>
                <h1 className={classes.titles}>
                    Comment
                </h1>
                <PostTheTernCafe />
                </CardContent>
            </Card>
        </Box>
    )
}

export default Foresto