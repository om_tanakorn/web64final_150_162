import React from 'react'

import { Box } from '@mui/material'
import { makeStyles } from '@mui/styles'
import ImageCard from './ImageCard'
import cafe from '../static/cafe'

import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageCard: {
    fontFamily: 'Nunito',
    underline: 'none'
  }
}))

const ColorButton = styled(Button)(({ theme }) => ({
  fontFamily: 'Nunito',
}))

export default function () {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Button component={Link} to='TheTernCafe'>
        <ImageCard cafe={cafe[1]} />
      </Button>
      <Button component={Link} to='Foresto'>
        <ImageCard cafe={cafe[0]} />
      </Button>
    </Box>
  );
}