import React from 'react'
import { List, TextField, Button } from '@mui/material'
import CommentTheTernCafe from '../Comments/CommentTheTernCafe'
import SendIcon from '@mui/icons-material/Send'

function Post() {
    return (
        <div className="post">
            <div className="post__header">
                <List sx={{ backgroundColor: 'white' }}>
                    <div className="post__comments">
                        <CommentTheTernCafe />
                    </div>
                    <form className="post__form">
                    <TextField
                        label="add comment"
                        size="small"
                        variant="outlined"
                        className="post__input"
                        placeholder="add comment"
                    />
                    <Button
                        variant="contained"
                        size="small"
                        endIcon={<SendIcon />}
                    >
                        Send
                    </Button>
                    </form>
                </List>
            </div>
        </div>
    )
}

export default Post