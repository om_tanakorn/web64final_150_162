import React from 'react'
import { makeStyles } from '@mui/styles'
import { List, ListItem, ListItemText, ListItemAvatar, Avatar, Typography } from '@mui/material'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: 'white'
  },
  inline: {
    display: 'inline',
  },
}));

function CommentTheTernCafe() {
    const classes = useStyles();
    return (
        <List className={classes.root}>
            <ListItem alignItems="flex-start">
                <ListItemAvatar>
                    <Avatar alt="Remy Sharp" src="https://images.unsplash.com/photo-1609220361638-14ceb45e5e1e?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max" />
                </ListItemAvatar>
                <ListItemText
                 primary="username"
                 secondary={
                    <React.Fragment>
                    <Typography
                        component="span"
                        variant="body2"
                        className={classes.inline}
                        color="textPrimary"
                    >
                        good atmosphere
                        Price per head: 101 - 250 baht
                        Favorite dish: Spaghetti with salted egg squid
                        Cafes + restaurants in Hat Yai The shop is wide and comfortable. The shop has a large window. so it looks more open Suitable for days that want to relax
                        There is a wide variety of food to choose from. This time I ordered all of them like the salted egg squid spaghetti. Because after eating, it's very fitting. As for rice with condensed egg, tom yum seafood, I still think it's not the best. May have expected a lot because it is the signature of the shop.
                    </Typography>
                    </React.Fragment>
                 }
                />
            </ListItem>
        </List>
    )
}

export default CommentTheTernCafe