import React from 'react'

import { Box, AppBar, Toolbar } from '@mui/material'
import { makeStyles } from '@mui/styles'

import CoffeeIcon from '@mui/icons-material/Coffee'
import { brown } from '@mui/material/colors'
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '20vh',
        fontFamily: 'Nunito'
    },
    colorText: {
        color: '#563517'
    },
    h1: {
        flexGrow: '1'
    },
}))

function Header() {
    const classes = useStyles()
    return(
        <Box className={classes.root} id='header'>
            <AppBar style={{backgroundColor: 'transparent'}} elevation={0}>
                <Toolbar>
                    <h1 className={classes.h1}>
                        Coffee
                        <span className={classes.colorText}>Lover</span>
                        <CoffeeIcon sx={{ fontSize: 40, color: brown[700] }} />
                    </h1>
                </Toolbar>
            </AppBar>
        </Box>
    )
}

export default Header